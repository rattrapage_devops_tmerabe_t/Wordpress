#!/bin/bash


# MYSQL 

if [ -d /app/mysql ]; then
  echo "[i] MySQL directory already present, skipping creation"
else
  echo "[i] MySQL data directory not found, creating initial DBs"

  mysql_install_db --user=root > /dev/null

    MYSQL_ROOT_PASSWORD=111111
    echo "[i] MySQL root Password: $MYSQL_ROOT_PASSWORD"


  MYSQL_DATABASE=wordpress
  MYSQL_USER=wordpressuser
  MYSQL_PASSWORD=wordpressuser

  if [ ! -d "/run/mysqld" ]; then
    mkdir -p /run/mysqld
  fi

  tfile=`mktemp`
  if [ ! -f "$tfile" ]; then
      return 1
  fi

  cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY "$MYSQL_ROOT_PASSWORD" WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD("") WHERE user='root' AND host='localhost';
EOF

  if [ "$MYSQL_DATABASE" != "" ]; then

    if [ "$MYSQL_USER" != "" ]; then
      echo "[i] Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
	  echo "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD'";
      echo "GRANT ALL ON \`$MYSQL_DATABASE\`.* to '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD';" >> $tfile
    fi
  fi

  /usr/bin/mysqld --user=root --bootstrap --verbose=0 < $tfile
  rm -f $tfile
fi

exec /usr/bin/mysqld --user=root &

	WP_R_LOC="fr_FR"
	WP_R_TITLE="Titre Provisoire"
	WP_R_ADMIN="admin"
	WP_R_PWD="admin"
	WP_R_MAIL="test@test.fr"
	if [ "${WP_LOCAL:-}" ];  then
	WP_R_LOC="${WP_LOCAL}"
	fi
	if [ "${WP_TITLE:-}" ];  then
	WP_R_TITLE="${WP_TITLE}"
	fi
	 if [ "${WP_ADMIN:-}" ]; then
	WP_R_ADMIN="$WP_ADMIN"
	fi
	 if [ "${WP_ADMIN_PWD:-}"  ]; then
	WP_R_PWD="$WP_ADMIN_PWD"
	fi
	 if [ "${WP_ADMIN_MAIL:-}" ]; then
	WP_R_MAIL="$WP_ADMIN_MAIL"
	fi
	
cd /var/www/html
wp core --allow-root download --locale="$WP_R_LOC" --force
wp core config --allow-root --dbhost=127.0.0.1 --dbname="$MYSQL_DATABASE" --dbuser="$MYSQL_USER" --dbpass="$MYSQL_PASSWORD" --skip-check --extra-php <<PHP
define( 'WP_DEBUG', true );
PHP
wp db create --allow-root 
echo "Instancie wordpress avec comme parametre : url='http://127.0.0.1' title='$WP_R_TITLE' admin_user='$WP_R_ADMIN' admin_email='$WP_R_MAIL' admin_password='$WP_R_PWD'";
wp core install --allow-root --url="http://127.0.0.1" --title="$WP_R_TITLE" --admin_user="$WP_R_ADMIN" --admin_email="$WP_R_MAIL" --admin_password="$WP_R_PWD"
nginx
exec "$@"